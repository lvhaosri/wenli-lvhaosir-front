// 统一请求路径前缀在libs/axios.js中修改
import { getRequest,urlGetRequest, postRequest, putRequest, deleteRequest, importRequest, uploadFileRequest } from '@/libs/axios';

// 楼栋信息
export const initBuild = (params) => {
  return getRequest('/build/queryAll', params)
}
export const getBuildList = (params) => {
    return getRequest('/build/getAllByPage', params)
}

export const addBuild = (params) => {
  return postRequest('/build/add', params)
}

export const updateBuild = (params) => {
  return putRequest('/build/update', params)
}

export const deleteBuild = (ids, params) => {
    return deleteRequest(`/build/delByIds/${ids}`, params)
}

// 学生信息
export const getStudentAll = (params) => {
  return getRequest('/student/getAll', params)
}
export const getStudentList = (params) => {
  return getRequest('/student/getAllByPage', params)
}
export const getStudentByCardId = (id, params) => {
  return getRequest(`/student/queryByCardId/${id}`, params)
}
export const updateExceptStatus = (params) => {
  return putRequest('/student/updateExceptStatus', params)
}
export const updateStatusDisable = (params) => {
  return putRequest('/student/disable', params)
}
export const updateStatusEnable = (params) => {
  return putRequest('/student/enable', params)
}
export const resetPassword = (params) => {
  return putRequest('/student/resetPassword', params)
}
export const saveStudent = (params) => {
  return postRequest('/student/save', params)
}
export const updateRoomChiefById = (id, params) => {
  return putRequest(`/student/updateRoomChiefById/${id}`, params)
}

// 班级信息
export const initClazz = (params) => {
  return getRequest('/clazz/queryAll',params)
}
export const loadClazzByDeparmentId = (id, params) => {
  return getRequest(`/clazz/loadByDepartmentId/${id}`, params)
}

// 宿舍信息
export const loadRoomByBuildId = (id, params) => {
  return getRequest(`/room/queryByBuildId/${id}`, params)
}

// 周记信息
export const getWeekTextList = (params) => {
  return getRequest('/weekText/getAllByPage', params)
}
// 删除周记
export const deleteWeekText = (ids, params) => {
  return deleteRequest(`/weekText/delByIds/${ids}`, params)
}

// 班级信息
export const getClassList = (params) => {
  return getRequest('/clazz/getAllByPage', params)
}
export const updateClassById = (params) => {
  return putRequest('/clazz/updateById', params)
}
export const saveClass = (params) => {
  return postRequest('/clazz/save', params)
}
export const deleteClass = (ids, params) => {
  return deleteRequest(`/clazz/delByIds/${ids}`, params)
}

// 教师信息
export const initTeacher = (params) => {
  return getRequest('/user/queryAll', params)
}

export const loadTeacherByDepartmentId = (ids, params) => {
  return getRequest(`/user/queryByDepartmentId/${ids}`, params)
}

// 宿舍信息
export const getRoomList = (params) => {
  return getRequest('/room/getAllByPage', params)
}
export const saveRoom = (params) => {
  return postRequest('/room/save', params)
}
export const updateRoomById = (params) => {
  return putRequest('/room/updateById', params)
}
export const deleteRoom = (ids, params) => {
  return deleteRequest(`/room/delByIds/${ids}`, params)
}
